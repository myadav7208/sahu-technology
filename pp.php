<?php
    session_start(); 
   
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PrivacyPolicy-Freeshoping</title>
    
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">

  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="user-menu">
                        <ul>
                            <?php if(isset($_SESSION['user'])) { ?>
                             <li><a href="logout.php" > Logout</a></li>
                                <li><h5>welcome <?php echo $_SESSION['name'] ?></h5></li> 
                            <?php } else{ ?>   
                            <li><a href="#Login" data-toggle="modal" data-target="#Login"> Login</a></li>
                                <div class="modal fade" id="Login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                        <div class="modal-body">
                                            <form action="login.php" method="POST">
                                                <input type="text" id="login" class="fadeIn second" name="email" placeholder="UserId">
                                                <input type="password" id="password" class="fadeIn third" name="pass" placeholder="password">
                                                <input type="submit" class="fadeIn fourth" value="Login" name="submit1">
                                            </form>
                                            

                                    </div>
                                 <div class="modal-footer">
                                    <a class="underlineHover" href="#" style="padding-right: 450px">Forgot Password ?</a>
                                    <a class="underlineHover" href="#Register" onclick="$('#Login').modal('hide')" data-toggle="modal" data-target="#Register" style="padding-right: 450px">Register</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                     </div>
                            <li><a href="#Register" data-toggle="modal" data-target="#Register"> Register</a></li>
                                    <div class="modal fade" id="Register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Register</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                        <div class="modal-body">
                                            <form action="register.php" method="POST">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                            <input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="last_name" id="last_name" class="form-control input-sm" placeholder="Last Name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address">
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
                                    </div>
                                </div>
                            </div>
                            
                            <input type="submit" value="Register" class="btn btn-info btn-block" name="submit">
                        
                        </form>
                                            

                                    </div>
                                 <div class="modal-footer">
                                    <a class="underlineHover" href="#" style="padding-right: 450px">Forgot Password ?</a>
                                    <a class="underlineHover" href="#" data-toggle="modal" data-target="#Login" style="padding-right: 465px" onclick="$('#Register').modal('hide')">Login</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                     </div>
                    <?php } ?>
                    <li><a href="career.php" > Career With Us</a></li>

                    </ul>
                    </div>
                </div>
                 <div class="row">
                <div class="col-md-4">
                    <div class="user-menu">
                        <ul>
                 <li><a href="https://www.facebook.com/freeshoppingpage" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/freeshoppingco" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCaAkF7F98rJ2rXdTf66v5hg" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="https://in.linkedin.com/in/freeshopping" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="https://www.instagram.com/freeshopping.co/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://in.pinterest.com/freeshoppingin/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="http://freeshopping.co/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="https://www.slideshare.net/freeshoppingonline1" target="_blank"><i class="fa fa-foursquare"></i></a></li>
                        </ul>
                </div>
                </div>
            </div>
                </div>
            </div>
        </div>
    </div>  <!-- End header area -->
    
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="./"><img src="img/logo.png"></a></h1>
                    </div>
                </div>
                
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="freebies.php">Freebies</a></li>
                        <li><a href="mobile_recharge.php">Mobile Recharge Offer</a></li>
                        <li><a href="electronic.php">Electronics Offer</a></li>
                        <li><a href="restaurant.php">Restaurant Offer</a></li>
                        <li><a href="travel_offer.php">Travels Offer</a></li>
                        <li><a href="groceries_offer.php">Groceries Offer</a></li>
                        <li><a hraf="student_offer.php" class="btn btn-warning btn-xs" style="border-radius: 30px;">Student Offer</a></li>
                    </ul>
                </div>  
            </div>
        </div>
    </div>

    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Privacy Policy</h2>
                    </div>
                </div>
            </div>
        </div>
    </div><br/><br/>

    <div class="container">
        <center><h2 style="color:tomato"><u>Privacy Policy & Opt Out</u></h2></center>
        <p>Here At Freeshopping.Co We Recognize That Your Privacy Is Very Important, And To Keep You Informed About Our Privacy Practices, We Ask That You Please Read The Guidelines Described Below. Keep In Mind That Because Freeshopping.Co Is A Growing Website And Internet Technologies Are Constantly Evolving, These Guidelines Are Subject To Change And Any Said Changes Will Be Posted On This Page. Any Material Changes From Current Practices Will Be Posted On The Main Page Of This Website To Serve As Notice. Freeshopping Is Not Responsible For The Content Or The Privacy Policies Of Websites To Which It May Link.</p><br/>
 <h4 style="color:tomato">Personal Information</h4>
 <p>During Registration For Membership, We May Collect Personal Information Such As Your Name, Email Address, Birth Date, Gender, Zip Code, Occupation, Industry, And Personal Interests. For Some Financial Products And Services We May Also Ask For More Information. We Usually Collect Personal Information Directly From You Although Sometimes We May Use Agents Or Service Providers To Do This For Us. When You Are Online, We Collect Information Regarding The Pages Within Our Network That You Visit. We May Also Acquire Lists From Other Sources, Both From Other Companies And From Other Public Documents. For Each Visitor Of Our Website, Our Web Server Automatically Recognizes Only The Consumer’s Domain Name, But Not The E-Mail Address (Where Possible). We Do Not Collect Information On Consumers Who Browse Our Website.</p>

 <h4 style="color:tomato">Location Information</h4>
 <p>Use Of Our Mobile Application May, At Your Election, Include Location-Based Services. When You Use The App, You May Transmit Information About Your Location To Freeshopping.Co. That Information Will Be Used To Offer You Deals And Coupons That Are Close To Your Geographic Location.</p>

<h4 style="color:tomato">Use Of Information</h4>
 <p>We Use Your Information To Provide Our Services To You, To Fulfill Administrative Functions Associated With These Services, To Enter Into Contracts With You Or Third Parties, And For Marketing And Client Relationship Purposes. You May Receive E-Mails From Us To Let You Know About New Products, Services, And Upcoming Events That May Interest You. If You Do Not Want To Receive E-Mails From Us Or You Want To Make Changes To Your E-Mail Settings, Please Change Your Setting Under Your Setting Tab. We Will Never Sell Or Distribute Your E-Mail Address To Other Companies Or Organizations, And Your Account Password Will Always Remain Protected. We May Use Customer Information For New, Unanticipated Uses Not Previously Disclosed In Our Privacy Notice. If Our Information Practices Change At Some Time In The Future We Will Post The Policy Changes To Our Website To Notify You Of These Changes, And Provide You With The Ability To Opt Out Of These New Uses. If You Are Concerned About How Your Information Is Used, You Should Check Our Privacy Policy Periodically.</p>

 <h4 style="color:tomato">Cookies</h4>
 <p>A Cookie Is A Piece Of Data That A Website Transfers To An Individual’s Hard Drive For Recordkeeping Purposes. Cookies Can Facilitate A User’s Ongoing Access To And Use Of A Site, And They Allow Us To Track Usage Patterns And To Compile Data That Can Help Us Improve Our Content And Target Advertising. If You Do Not Want Information Collected Through The Use Of Cookies, There Is A Simple Procedure In Most Browsers That Allows You To Deny Or Accept The Cookie Feature. But You Should Note That Cookies May Be Necessary To Provide You With Features Such As Merchandise Transactions Or Registered Services.</p>

 <h4 style="color:tomato">Use Of Aggregate Data</h4>
 <p>We May Collect Certain Non-Personal Information To Optimize Our Website For Your Computer (E.G., The Identity Of Your Internet Browser, The Type Of Operating System You Use, Your IP Address, And The Domain Name Of Your Internet Service Provider). We May Use Such Non-Personal Information For Internal Purposes, Including But Not Limited To Improving The Content Of Our Website. Freeshopping.Co May Use Personally Identifiable Information In Aggregate Form To Improve Our Goods And Services, Including Our Website, And To Make Them More Responsive To The Needs Of Our Customers. This Statistical Compilation And Analysis Of Information May Also Be Used By Freeshopping.Co, Or Provided To Others As A Summary Report For Marketing, Advertising, Or Research Purposes.</p>

 <h4 style="color:tomato">Confidentiality And Security</h4>
 <p>We Limit Access To Personal Information About You To Employees For Whom We Reasonably Believe It Is Necessary In Order To Provide Products Or Services To You. We Have Physical, Electronic, And Procedural Safeguards That Comply With Federal Regulations To Protect Against The Loss, Misuse, Or Alteration Of Information That We Have Collected From You At Our Website.</p>

 <h4 style="color:tomato">What Else You Should Know About Privacy On The Internet</h4>
 <p>Remember To Close Your Browser When You Have Finished Your Freeshopping.Co Session. This Is To Ensure That Others Cannot Access Your Personal Information And Correspondence If You Share A Computer With Someone Else Or Are Using A Computer In A Public Place, Like A Library Or Internet Cafe. You As An Individual Are Responsible For The Security Of And Access To Your Own Computer.
Whenever You Voluntarily Disclose Personal Information Over The Internet, This Information Can Be Collected And Used By Others. In Short, If You Post Personal Information In Publicly Accessible Online Forums, You May Receive Unsolicited Messages From Other Parties In Return.
Ultimately, You Are Solely Responsible For Maintaining The Secrecy Of Your Username And Passwords And Any Account Information.
Please Be Careful And Responsible Whenever You Are Using The Internet.
If You Have Any Questions About Our Privacy Policy, Or You Would Like To Cancel Your Account, Please Contact Us At Freeshopping.Official@Gmail.Com Or Info@Freeshopping.Co</p>

<h4 style="color:tomato">What Information Do We Collect?</h4>
 <p>We Collect Information From You When You Subscribe To Our Newsletter.
When Ordering Or Registering On Our Site, As Appropriate, You May Be Asked To Enter Your: E-Mail Address. You May, However, Visit Our Site Anonymously.
This Website Uses The Google AdWords Remarketing Service To Advertise On Third Party Websites (Including Google) To Previous Visitors To Our Site. It Could Mean That We Advertise To Previous Visitors Who Haven’t Completed A Task On Our Site, For Example Using The Contact Form To Make An Enquiry. This Could Be In The Form Of An Advertisement On The Google Search Results Page, Or A Site In The Google Display Network. Google, As A Third Party Vendor, Uses Cookies To Serve Ads On Your Site. Google’s Use Of The DART Cookie Enables It To Serve Ads To Your Users Based On Their Visit To Your Sites And Other Sites On The Internet. Users May Opt Out Of The Use Of The DART Cookie By Visiting The Google Ad And Content Network Privacy Policy.</p>

<h4 style="color:tomato">What Do We Use Your Information For?</h4>
 <p>Any Of The Information We Collect From You May Be Used In One Of The Following Ways:
; To Personalize Your Experience
(Your Information Helps Us To Better Respond To Your Individual Needs)
; To Improve Our Website
(We Continually Strive To Improve Our Website Offerings Based On The Information And Feedback We Receive From You)
; To Send Periodic Emails
The Email Address You Provide May Be Used To Send You Information, Respond To Inquiries, And/Or Other Requests Or Questions.
Note: If At Any Time You Would Like To Unsubscribe From Receiving Future Emails, We Include Detailed Unsubscribe Instructions At The Bottom Of Each Email.</p>

<h4 style="color:tomato">How Do We Protect Your Information?</h4>
 <p>We Implement A Variety Of Security Measures To Maintain The Safety Of Your Personal Information When You Enter, Submit, Or Access Your Personal Information.</p>

<h4 style="color:tomato">Do We Use Cookies?</h4>
 <p>Yes (Cookies Are Small Files That A Site Or Its Service Provider Transfers To Your Computers Hard Drive Through Your Web Browser (If You Allow) That Enables The Sites Or Service Providers Systems To Recognize Your Browser And Capture And Remember Certain Information
We Use Cookies To Understand And Save Your Preferences For Future Visits.</p>

<h4 style="color:tomato">Do We Disclose Any Information To Outside Parties?</h4>
 <p>We Do Not Sell, Trade, Or Otherwise Transfer To Outside Parties Your Personally Identifiable Information. This Does Not Include Trusted Third Parties Who Assist Us In Operating Our Website, Conducting Our Business, Or Servicing You, So Long As Those Parties Agree To Keep This Information Confidential. We May Also Release Your Information When We Believe Release Is Appropriate To Comply With The Law, Enforce Our Site Policies, Or Protect Ours Or Others Rights, Property, Or Safety. However, Non-Personally Identifiable Visitor Information May Be Provided To Other Parties For Marketing, Advertising, Or Other Uses.</p>

<h4 style="color:tomato">Third Party Links</h4>
 <p>Occasionally, At Our Discretion, We May Include Or Offer Third Party Products Or Services On Our Website. These Third Party Sites Have Separate And Independent Privacy Policies. We Therefore Have No Responsibility Or Liability For The Content And Activities Of These Linked Sites. Nonetheless, We Seek To Protect The Integrity Of Our Site And Welcome Any Feedback About These Sites.</p>

<h4 style="color:tomato">Childrens Online Privacy Protection Act Compliance</h4>
 <p>We Are In Compliance With The Requirements Of COPPA (Childrens Online Privacy Protection Act), We Do Not Collect Any Information From Anyone Under 13 Years Of Age. Our Website, Products And Services Are All Directed To People Who Are At Least 13 Years Old Or Older.</p>

<h4 style="color:tomato">Use Of Site Browsing History</h4>
 <p>By Using Website FreeShopping.Co, You Consent To Collection, Storage, And Use Of Your Browsing History On This Website. We Share This Information With Trusted 3rd Parties That Use It To Show You Marketing Communications Of Interest Only. While No Personally Identifiable Information Is Shared, We Respect Your Right To Privacy Hence This Transparent Disclosure. In Addition, We Respect Your Right To Opt Out. If You Want To Opt Out From This Program, Please Click On The Link Below And Select The Appropriate Option On The Destination Page –</p>

<h4 style="color:tomato">Opt Out Page.</h4>
 <p>In Addition, Most Setup Your Browser Software To Reject 3rd Party Cookies. In Addition, You Can Opt Out Of Several Similar Programs By Visiting The Network Advertising Initiative Gateway Opt-Out Site.</p>
 <a href="Http://Www.Freeshopping.Co">Http://Www.Freeshopping.Co</a>
       <br/><br/><br/><br/>
    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="location.href='contact.php';">Contact Us</button>

    </div><br/><br/>

    <div class="footer-top-area" style="padding-top:0px;">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-about-us">
                        <h2>F<span>ree shopping</span></h2>
                        <p>FreeShopping Follows His Heart, Regardless Of Society’s Resistance And Rules!
                            While He Walks His Own Path, A FreeShopping Sees Limitless Inspiration To Change The World For Better And Make A Difference.
                            In All Our Diversities, There’s A Bit Of FreeShopping In Each One Of Us!</p>
                        <div class="footer-social">
                            <a href="https://www.facebook.com/freeshoppingpage" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/freeshoppingco" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCaAkF7F98rJ2rXdTf66v5hg" target="_blank"><i class="fa fa-youtube"></i></a>
                            <a href="https://in.linkedin.com/in/freeshopping" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <a href="https://www.instagram.com/freeshopping.co/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="https://in.pinterest.com/freeshoppingin/" target="_blank"><i class="fa fa-pinterest"></i></a>
                            <a href="http://freeshopping.co/" target="_blank"><i class="fa fa-google-plus"></i></a>
                            <a href="https://www.slideshare.net/freeshoppingonline1" target="_blank"><i class="fa fa-foursquare"></i></a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">GET TO KNOW US </h2>
                        <ul>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="ourteam.php">Our Team</a></li>
                            <li><a href="faq.php">FAQ's</a></li>
                            <li><a href="testimonial.php">Testimonials</a></li>
                            <li><a href="career.php">Career With Us</a></li>
                            <li><a href="media.php">Media Contact</a></li>
                            <li><a href="invest.php">Investors</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">SUPPORT</h2>
                        <ul>
                            <li><a href="advertise.php">Advertise With Us</a></li>
                            <li><a href="campus.php">Campus Ambassador</a></li>
                            <li><a href="feedback.php">Feedback</a></li>
                            <li><a href="giftcard.php">Win A Gift Cards</a></li>
                            <li><a href="tandc.php">Terms & Conditions</a></li>
                            <li><a href="pp.php">Privacy Policy</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-newsletter">
                        <h2 class="footer-wid-title">Never Miss Out Exclusive Deals !</h2>
                        <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                        <div class="newsletter-form">
                            <form action="#">
                                <input type="email" placeholder="Type your email">
                                <input type="submit" value="Subscribe">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p>Copyright 2015-2020. Freeshopping. All Rights Reserved. All Content, Trademarks And Logos Are Copyright Of Their Respective Owners. <a href="https://www.sahutechnologies.com/" target="_blank"> Sahu Technologies</a></p>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    
    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
    <script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>