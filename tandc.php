<?php
    session_start(); 
    
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>T & C-Freeshoping</title>
    
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">

  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="user-menu">
                        <ul>
                            <?php if(isset($_SESSION['user'])) { ?>
                             <li><a href="logout.php" > Logout</a></li>
                                <li><h5>welcome <?php echo $_SESSION['name'] ?></h5></li> 
                            <?php } else{ ?>   
                            <li><a href="#Login" data-toggle="modal" data-target="#Login"> Login</a></li>
                                <div class="modal fade" id="Login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                        <div class="modal-body">
                                            <form action="login.php" method="POST">
                                                <input type="text" id="login" class="fadeIn second" name="email" placeholder="UserId">
                                                <input type="password" id="password" class="fadeIn third" name="pass" placeholder="password">
                                                <input type="submit" class="fadeIn fourth" value="Login" name="submit1">
                                            </form>
                                            

                                    </div>
                                 <div class="modal-footer">
                                    <a class="underlineHover" href="#" style="padding-right: 450px">Forgot Password ?</a>
                                    <a class="underlineHover" href="#Register" onclick="$('#Login').modal('hide')" data-toggle="modal" data-target="#Register" style="padding-right: 450px">Register</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                     </div>
                            <li><a href="#Register" data-toggle="modal" data-target="#Register"> Register</a></li>
                                    <div class="modal fade" id="Register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Register</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                        <div class="modal-body">
                                            <form action="register.php" method="POST">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                            <input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="last_name" id="last_name" class="form-control input-sm" placeholder="Last Name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address">
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
                                    </div>
                                </div>
                            </div>
                            
                            <input type="submit" value="Register" class="btn btn-info btn-block" name="submit">
                        
                        </form>
                                            

                                    </div>
                                 <div class="modal-footer">
                                    <a class="underlineHover" href="#" style="padding-right: 450px">Forgot Password ?</a>
                                    <a class="underlineHover" href="#" data-toggle="modal" data-target="#Login" style="padding-right: 465px" onclick="$('#Register').modal('hide')">Login</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                     </div>
                    <?php } ?>
                    <li><a href="career.php" > Career With Us</a></li>

                    </ul>
                    </div>
                </div>
                 <div class="row">
                <div class="col-md-4">
                    <div class="user-menu">
                        <ul>
                 <li><a href="https://www.facebook.com/freeshoppingpage" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/freeshoppingco" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCaAkF7F98rJ2rXdTf66v5hg" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="https://in.linkedin.com/in/freeshopping" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="https://www.instagram.com/freeshopping.co/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://in.pinterest.com/freeshoppingin/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="http://freeshopping.co/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="https://www.slideshare.net/freeshoppingonline1" target="_blank"><i class="fa fa-foursquare"></i></a></li>
                        </ul>
                </div>
                </div>
            </div>
                </div>
            </div>
        </div>
    </div>  <!-- End header area -->
    
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="./"><img src="img/logo.png"></a></h1>
                    </div>
                </div>
                
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                      <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="freebies.php">Freebies</a></li>
                        <li><a href="mobile_recharge.php">Mobile Recharge Offer</a></li>
                        <li><a href="electronic.php">Electronics Offer</a></li>
                        <li><a href="restaurant.php">Restaurant Offer</a></li>
                        <li><a href="travel_offer.php">Travels Offer</a></li>
                        <li><a href="groceries_offer.php">Groceries Offer</a></li>
                        <li><a hraf="student_offer.php" class="btn btn-warning btn-xs" style="border-radius: 30px;">Student Offer</a></li>
                    </ul>
                </div>  
            </div>
        </div>
    </div>

    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Terms & Conditions</h2>
                    </div>
                </div>
            </div>
        </div>
    </div><br/><br/>

    <div class="container">
        <center><h2 style="color:tomato"><u>Terms Of Service</u></h2></center><br/><br/>
        <h4 style="color:tomato">Terms Of Use</h4>
        <p >We Have Drawn Up An Agreement Of Our Terms And Conditions To Serve You Better And To Clarify Our Service. You Must Read And Accept All Of The Terms And Conditions Of This Agreement And In The Privacy Policy In Order To Use Our Service. If You Do Not Agree To Be Bound By The Terms After You Read This Agreement, You May Not Use Our Service.</p><br/>
 <h4 style="color:tomato">The Service And Terms</h4>
 <p >Freeshopping.Co Provides A Collection Of Online Resources, Including Classified Ads, Affiliate Ads, Blogs,  Forums, Business Directory Etc. . The Service Is Provided By Freeshopping.Co, And Is Subject To The Following Terms Of Use, Which May Be Updated By Freeshopping.Co From Time To Time. Freeshopping.Co  Will Provide Notice Of Materially Significant Changes To The Terms, By Posting A Notice On The Freeshopping.Co  Homepage. You Must Be At Least 12 Years Of Age To Use This Service. By Using Our  Service In Any Way, You Are Agreeing To Comply With These Terms.</p>

 <h4 style="color:tomato">Content</h4>
 <p>You Understand That All Postings, Messages, Text, Files, Images, Photos, Video, Sounds, Or Other Materials (“Content”) Posted On, Transmitted Through, Or Linked From The Service, Are The Sole Responsibility Of The Person From Whom Such “Content” Has  Originated. You Are Entirely Responsible For All Content That You Post Or Email On The Service. Freeshopping.Co Does Not Control, And Is Not Responsible For Any Content Made Available Through The Service. By Using The Service, You May Be Exposed To Content That Is Offensive, Indecent, Inaccurate, Misleading, Or Otherwise Objectionable. Furthermore, The Freeshopping.Co  Website And Content Available Through The Website May Contain Links To Other Websites. Freeshopping.Co Makes No Representation Or Warranty As To The Accuracy, Completeness Or Authenticity Of The Information Contained In Any Such Site. Your Linking To Any Other Websites Is At Your Own Risk And We Recommend That You Do So Carefully. Freeshopping.Co  Will Not Be Liable In Any Way For Any Content Or For Any Loss Or Damage Of Any Kind Incurred As A Result Of The Use Of Any Content Posted, Emailed Or Otherwise Made Available Via The Service. Freeshopping.Co  Does Not Pre-Screen Or Approve Content, But It Has The Right (But Not The Obligation) In Its Sole Discretion To Refuse, Delete Or Move Any Content That Is Available Via The Service, For Violating The Letter Or Spirit Of The Terms Or For Any Other Reason.
By Continuing To Use Freeshopping.Co  You Hereby Grant To Freeshopping.Co A Non-Revocable, Non-Exclusive, Transferable, Royalty-Free And Sub-Licensable Worldwide License To All Text, Photographs, Film, Audio, Or Other Recordings, Still Or Moving That You Submit (“Media”) For Freeshopping.Co Unqualified Use Or Modification In Any Medium Including Digital, Electronic, Print, Television, Film, Radio And Other Medium Now Known Or To Be Invented For Any Purpose (Except Pornographic Or Defamatory) Which May Include, Among Others, Advertising, Promotion And Marketing. You Agree That The Media May Be Combined With Other Images, Text, Graphics, Film, Audio, Audio-Visual Works; And May Be Cropped, Altered Or Modified. You Acknowledge And Agree That You Have No Further Right To Additional Consideration Or Accounting, And That You Will Make No Further Claim For Any Reason To Freeshopping.Co. You Acknowledge And Agree That This Release Is Binding Upon Your Heirs And Assigns.</p>

<h4 style="color:tomato">Copyrights And Trademarks</h4>
 <p>Content Displayed On Or Through The Website  Is Protected By Copyright As A Collective Work And/Or Compilation, Pursuant To Copyrights Laws, And International Conventions. Freeshopping.Co, The Freeshopping.Co  Logo, And Icons And Marks Identifying Freeshopping.Co Products And Services Are Trademarks Of Freeshopping.Co And May Not Be Used Without The Written Consent Of The Freeshopping.Co. You May Not Copy, Reproduce, Distribute, Or Create Derivative Works Of The Website Without A Written Authorisation From Freeshopping.Co . You May Not Reverse Engineer, Decompile, Alter, Modify, Disassemble, Or Otherwise Attempt To Derive Source Code From The Website. Freeshopping.Co Respects The Intellectual Property Of Others And We Require That Our Users Do The Same. You May Not Post, Distribute, Or Reproduce In Any Way Any Copyrighted Material, Trademarks, Or Other Proprietary Information That You Do Not Have Legal Authorisation To Use.
You Agree Not To Use Or Launch Any Automated System, Including Without Limitation, “Robots”, “Spiders”, “Offline Readers”, Etc., That Accesses Freeshopping.Co  In A Manner That Sends More Request Messages To The Freeshopping.Co Servers In A Given Period Of Time Than A Human Can Reasonably Produce In The Same Period By Using A Conventional On-Line Web Browser. Notwithstanding The Foregoing, Freeshopping.Co  Grants The Operators Of Public Search Engines Permission To Use Spiders To Copy Materials From The Site For The Sole Purpose Of Creating Publicly Available Searchable Indices Of The Materials, But Not Caches Or Archives Of Such Materials. Freeshopping.Co  Reserves The Right To Revoke These Exceptions Either Generally Or In Specific Cases. You Agree Not To Collect Or Harvest Any Personally Identifiable Information, Including Account Names, From Freeshopping.Co, Nor To Use The Communication Systems Provided By Freeshopping.Co  For Any Commercial Solicitation Purposes.
If You Are A Competitor To Freeshopping.Co, You Hereby Acknowledge That Freeshopping.Co Is Private Property And You Are Not Allowed To Access Freeshopping.Co. Additionally, You Hereby Agree That You Will Leave Freeshopping.Co Immediately And Will Not Access It Further, Nor Will You Direct Others To Access Freeshopping.Co For Any Competitive Purposes.</p>

 <h4 style="color:tomato">Notification Of Claims Of Infringement</h4>
 <p>If You Believe That Your Work Has Been Copied In A Way That Constitutes Copyright Infringement, Or Your Intellectual Property Rights Have Been Otherwise Violated, Please Notify The Freeshopping.Co Agent For Notice Of Claims Of Copyright Or Other Intellectual Property Infringement (“Agent”). Please Provide Our Agent With The Following Notice:
Identify The Copyrighted Work Or Other Intellectual Property That You Claim Has Been Infringed;
Identify The Material On The Freeshopping.Co Site That You Claim Is Infringing, With Enough Detail So That We May Locate It On The Website;
A Statement By You That You Have A Good Faith Belief That The Disputed Use Is Not Authorised By The Copyright Owner, Its Agent, Or The Law;
A Statement By You Declaring Under Penalty Of Perjury, That (A) The Above Information In Your Notice Is Accurate, And (B) That You Are The Owner Of The Copyright Interest Involved Or That You Are Authorised To Act On Behalf Of That Owner;
Your Address, Telephone Number, And Email Address; And
Your Physical Or Electronic Signature.
Freeshopping.Co Will Remove The Infringing Posting(S), Subject To The The Procedures Outlined In The Digital Millennium Copyright Act (DMCA).</p>

 <h4 style="color:tomato">Privacy And Information Disclosure</h4>
 <p>Please Review Our Privacy Policy. Your Use Of The Freeshopping.Co Website Or The Service Signifies Acknowledgement Of And Agreement To Our Privacy Policy.</p>

 <h4 style="color:tomato">Conduct</h4>
 <p>You Agree Not To Post, Email, Or Otherwise Make Available Content:
That Is Unlawful, Harmful, Threatening, Abusive, Harassing, Defamatory, Pornographic, Libellous, Invasive Of Another’s Privacy, Or Harms Minors In Any Way;
That Harasses, Degrades, Intimidates Or Is Hateful Toward An Individual Or Group Of Individuals On The Basis Of Religion, Gender, Sexual Orientation, Race, Ethnicity, Age, Or Disability;
That Impersonates Any Person Or Entity, Including, But Not Limited To, A Freeshopping.Co Employee, Or Falsely States Or Otherwise Misrepresents Your Affiliation With A Person Or Entity;
That Includes Personal Or Identifying Information About Another Person Without That Person’s Explicit Consent.
That Is False, Deceptive, Misleading, Deceitful, Or Misinformation.
That Infringes Any Patent, Trademark, Trade Secret, Copyright Or Other Proprietary Rights Of Any Party, Or Content That You Do Not Have A Right To Make Available Under Any Law Or Under Contractual Or Fiduciary Relationships.
That Constitutes Or Contains “Affiliate Marketing,” “Link Referral Code,” “Junk Mail,” “Spam,” “Chain Letters,” “Pyramid Schemes,” Or Unsolicited Commercial Advertisement.
That Constitutes Or Contains Any Form Of Advertising Or Solicitation If (1) Posted In Areas Of The Freeshopping.Co Sites Which Are Not Designated For Such Purposes; Or (2) Emailed To Freeshopping.Co Users Who Have Requested Not To Be Contacted About Other Services, Products Or Commercial Interests.
That Includes Links To Commercial Services Or Web Sites, Except As Allowed In “Services”.
That Advertises Any Illegal Services Or The Sale Of Any Items The Sale Of Which Is Prohibited Or Restricted By Applicable Law, Including, Without Limitation Items The Sale Of Which Is Prohibited Or Regulated By Law.
That Contains Software Viruses Or Any Other Computer Code, Files Or Programs Designed To Interrupt, Destroy Or Limit The Functionality Of Any Computer Software Or Hardware Or Telecommunications Equipment;
That May Be Deemed To Be Engaging In Solicitation Activities In India That Refer Potential Customers To Out-Of-State Freeshopping.Co  Retailers, Including, But Not Limited To, Distributing Flyers, Coupons, Newsletters, And Other Printed Promotional Materials Or Electronic Equivalents, Verbal Soliciting (For Example, In-Person Referrals), Initiating Telephone Calls, And Sending Emails.
That Disrupts The Normal Flow Of Dialogue With An Excessive Number Of Messages To The Service, Or That Otherwise Negatively Affects Other Users’ Ability To Use The Service; Or
That Employs Misleading Email Addresses, Or Forged Headers Or Otherwise Manipulated Identifiers In Order To Disguise The Origin Of Content Transmitted Through The Service.
Furthermore, You Acknowledge That Freeshopping.Co Has Financial Relationships With Some Of The Merchants On The Site And That Freeshopping.Co May Be Compensated If You Choose To Utilise The Links On This Site And/Or Generate Sales For Such Merchants.</p>

 <h4 style="color:tomato">No Spam Policy</h4>
 <p>Any Unauthorised Use Of Freeshopping.Co Computer Systems, Including But Not Limited To The Sending Of Unsolicited Email Advertisements To Freeshopping.Co Email Addresses Or Through Freeshopping.Co Computer Systems, Is A Violation Of These Terms And Certain Laws, Including But Not Limited To The Computer Fraud And Abuse Act . Such Violations May Subject The Sender And His Or Her Agents To Civil And Criminal Penalties.</p>

<h4 style="color:tomato">Limitations Of Service</h4>
 <p>You Acknowledge That The Freeshopping.Co May Establish Limits Concerning Use Of The Service, Including The Maximum Number Of Days That Content Will Be Retained By The Service, The Maximum Number And Size Of Postings, Email Messages, Or Other Content That May Be Transmitted Or Stored By The Service, And The Frequency With Which You May Access The Service. You Agree That The Freeshopping.Co Has No Responsibility Or Liability For The Deletion Or Failure To Store Any Content Maintained Or Transmitted By The Service. You Acknowledge That The Freeshopping.Co Reserves The Right At Any Time To Modify Or Discontinue The Service (Or Any Part Thereof) With Or Without Notice, And That Freeshopping.Co Shall Not Be Liable To You Or To Any Third Party For Any Modification, Suspension Or Discontinuance Of The Service.</p>

<h4 style="color:tomato">Termination Of Service</h4>
 <p>You Agree That The Freeshopping.Co In Its Sole Discretion, Has The Right (But Not The Obligation) To Delete Or Deactivate Your Account, Block Your Email Or IP Address, Or Otherwise Terminate Your Access To Or Use Of The Service (Or Any Part Thereof), Immediately And Without Notice, And Remove And Discard Any Content Within The Service, For Any Reason, Including, Without Limitation, If The Freeshopping.Co Believes That You Have Violated These Terms. Further, You Agree That The Freeshopping.Co Shall Not Be Liable To You Or Any Third-Party For Any Termination Of Your Access To The Service. Further, You Agree Not To Attempt To Use The Service After Said Termination.</p>

<h4 style="color:tomato">Dealings With Organisations And Individuals</h4>
 <p>Your Interactions With Organisations And/Or Individuals Found On Or Through The Service, Including Payment And Delivery Of Goods Or Services, And Any Other Terms, Conditions, Warranties Or Representations Associated With Such Dealings, Are Solely Between You And Such Organisations And/Or Individuals. You Agree That The Freeshopping.Co Shall Not Be Responsible Or Liable For Any Loss Or Damage Of Any Sort Incurred As The Result Of Any Such Dealings. If There Is A Dispute Between Participants On This Site, Or Between Users And Any Third Party, You Understand And Agree That The Freeshopping.Co Is Under No Obligation To Become Involved. In The Event That You Have A Dispute With One Or More Other Users, You Hereby Release The Freeshopping.Co, Its Officers, Employees, Agents And Successors In Rights From Claims, Demands And Damages (Actual And Consequential) Of Every Kind Or Nature, Known Or Unknown, Suspected And Unsuspected, Disclosed And Undisclosed, Arising Out Of Or In Any Way Related To Such Disputes And / Or Our Service.</p>

<h4 style="color:tomato">Disclaimer Of Warranties</h4>
 <p>You Agree That Use Of The Freeshopping.Co  Website And The Service Is Entirely At Your Own Risk, Without Warranties Of Any Kind. All Express And Implied Warranties, Including, Without Limitation, The Warranties Of Merchantability, Fitness For A Particular Purpose, And Non-Infringement Of Proprietary Rights Are Expressly Disclaimed To The Fullest Extent Permitted By Law. To The Fullest Extent Permitted By Law, Freeshopping.Co  Disclaims Any Warranties For The Security, Reliability, Timeliness, Accuracy, And Performance Of The Freeshopping.Co  Website And The Service. To The Fullest Extent Permitted By Law, Freeshopping.Co  Disclaims Any Warranties For Other Services Or Goods Received Through Or Advertised On The Freeshopping.Co  Website Or The Sites Or Service, Or Accessed Through Any Links On The Freeshopping.Co  Website. To The Fullest Extent Permitted By Law, The Freeshopping.Co  Disclaims Any Warranties For Viruses Or Other Harmful Components In Connection With The Freeshopping.Co  Website Or The Service. Some Jurisdictions Do Not Allow The Disclaimer Of Implied Warranties. In Such Jurisdictions, Some Of The Foregoing Disclaimers May Not Apply To You Insofar As They Relate To Implied Warranties.</p>

<h4 style="color:tomato"> Limitations Of Liability</h4>
 <p>In No Event Shall The Freeshopping.Co  Be Liable For Direct, Indirect, Incidental, Special, Consequential Or Exemplary Damages , Resulting From Any Aspect Of Your Use Of The Freeshopping.Co  Website Or The Service, Whether The Damages Arise From Use Or Misuse Of The Freeshopping.Co  Website Or The Service, From Inability To Use The Freeshopping.Co  Website Or The Service, Or The Interruption, Suspension, Modification, Alteration, Or Termination Of The Freeshopping.Co  Website Or The Service. Such Limitation Shall Also Apply With Respect To Damages Incurred By Reason Of Other Services Or Products Received Through Or Advertised In Connection With The Freeshopping.Co  Website Or The Service Or Any Links On The Freeshopping.Co  Website, As Well As By Reason Of Any Information Or Advice Received Through Or Advertised In Connection With The Freeshopping.Co  Website Or The Service Or Any Links On The Freeshopping.Co  Website. These Limitations Shall Apply To The Fullest Extent Permitted By Law. In Some Jurisdiction, Limitations Of Liability Are Not Permitted. In Such Jurisdictions, Some Of The Foregoing Limitations May Not Apply To You.</p>

<h4 style="color:tomato"> Indemnity</h4>
 <p>You Agree To Indemnify And Hold The Freeshopping.Co  Website , Its Officers, Subsidiaries, Affiliates, Successors, Assigns, Directors, Officers, Agents, Service Providers, Suppliers And Employees, Harmless From Any Claim Or Demand, Including Reasonable Attorney Fees And Court Costs, Made By Any Third Party Due To Or Arising Out Of Content You Submit, Post Or Make Available Through The Service, Your Use Of The Service, Your Violation Of The Terms, Your Breach Of Any Of The Representations And Warranties Herein, Or Your Violation Of Any Rights Of Another.</p>

<h4 style="color:tomato">General Information</h4>
 <p>The Terms And The Relationship Between You And The Freeshopping.Co Shall Be Governed By The Laws Of India Without Regard To Its Conflict Of Law Provisions. You And The Freeshopping.Co Agree To Submit To The Personal And Exclusive Jurisdiction Of The Courts Located Within The County. The Failure Of The Freeshopping.Co  To Exercise Or Enforce Any Right Or Provision Of The Terms Shall Not Constitute A Waiver Of Such Right Or Provision. If Any Provision Of The Terms Is Found By A Court Of Competent Jurisdiction To Be Invalid, The Parties Nevertheless Agree That The Court Should Endeavour To Give Effect To The Parties’ Intentions As Reflected In The Provision, And The Other Provisions Of The Terms Remain In Full Force And Effect.</p>

<h4 style="color:tomato">COPYRIGHT COMPLAINTS</h4>
 <p>We Respect The Intellectual Property Of Others. If You Believe Your Work Has Been Copied In A Way That Constitutes Copyright Infringement Or Are Aware Of Any Infringing Material On The Web Site, Please Contact Us At The Address Listed Below Under The Heading “Contact Us.”
User-Provided Information And Content By Providing Information To, Communicating With, And/Or Placing Material On, The Web Site, Including For Example Communication During Any Registration, Communication On The Bulletin Board, Message Or Chat Area, Posting Any Resume Or Other Content, Placing Any Classified Advertisement, Entering Any Sweepstakes, Etc., You Represent And Warrant: (1) You Own Or Otherwise Have All Necessary Rights To The Content You Provide And The Rights To Use It As Provided In This Terms Of Service; (2) All Information You Provide Is True, Accurate, Current And Complete, And Does Not Violate These Terms Of Service; And, (3) The Content Will Not Cause Injury To Any Person Or Entity. Using A Name Other Than Your Own Legal Name Is Prohibited.
For All Such Information And Material, You Grant Us, A Royalty-Free, Perpetual, Irrevocable, Non-Exclusive Right And License To Use, Copy, Modify, Display, Archive, Store, Distribute, Reproduce And Create Derivative Works From Such Information, In Any Form, Media, Software Or Technology Of Any Kind Now Existing Or Developed In The Future. Without Limiting The Generality Of The Previous Sentence, You Authorize Us To Share The Information Across All Our Affiliated Web Sites, To Include The Information In A Searchable Format Accessible By Users Of The Web Site And Other Affiliated Web Sites, And To Use Your Name And Any Other Information In Connection With Its Use Of The Material You Provide. You Also Grant The Right To Use Any Material, Information, Ideas, Concepts, Know-How Or Techniques Contained In Any Communication You Send To Us For Any Purpose Whatsoever, Including But Not Limited To Developing, Manufacturing And Marketing Products Using Such Information. All Rights In This Paragraph Are Granted Without The Need For Additional Compensation Of Any Sort To You.
Please Note We Do Not Accept Unsolicited Materials Or Ideas For Use Or Publication, And Is Not Responsible For The Similarity Of Any Of Its Content Or Programming In Any Media To Materials Or Ideas Transmitted To Us. Should You Send Any Unsolicited Materials Or Ideas, You Do So With The Understanding No Additional Consideration Of Any Sort Will Be Provided To You, And You Are Waiving Any Claim Against Us And Our Affiliates Regarding The Use Of Such Materials And Ideas, Even If Material Or An Idea Is Used That Is Substantially Similar To The Idea You Sent</p>

<h4 style="color:tomato">Disputes And Jurisdiction</h4>
 <p>These Terms Of Service Will Be Governed By And Construed In Accordance With The Laws Of India Without Regard To Its Conflicts Of Law Provisions. You Hereby Agree That Any Cause Of Action You May Have With Respect To FreeShopping Must Be Filed In India Within Two Months Of The Time In Which The Events Giving Rise To Such Claim Began, Or You Agree To Waive Such Claim. You Agree No Such Claim May Be Brought As A Class Action. If For Any Reason Any Provision Of This Agreement Is Found Unenforceable, That Provision Shall Be Enforced To The Maximum Extent Permissible So As To Effect The Intent Of The Parties As Reflected In That Provision, And The Remainder Of The Agreement Shall Continue In Full Force And Effect. Any Failure Of FreeShopping To Enforce Or Exercise Any Provision Of This Agreement Or Related Right Shall Not Constitute A Waiver Of That Right Or Provision. The Section Titles Used In This Agreement Are Purely For Convenience And Carry With Them No Legal Or Contractual Effect. In The Event Of Termination Of This Agreement For Any Reason, You Agree The Following Provisions Will Survive: The Provisions Regarding Limitations On Your Use Of Content, The License(S) You Have Granted To FreeShopping, And All Other Provisions For Which Survival Is Equitable Or Appropriate.
In The Case Of A Conflict Between These Terms And The Terms Of Any Electronic Or Machine Readable Statement Or Policy (For Example, A P3P Electronic Privacy Policy), These Terms Of Service Shall Control. Similarly, In Case Of A Conflict Between These Terms And Our Privacy Policy, These Terms Of Service Control.
All Disputes Involving But Not Limited To Rights Conferred, Compensation, Refunds, And Other Claims Will Be Resolved Through A Two-Step Alternate Dispute Resolution Mechanism.
Stage 1: Mediation. In Case Of A Dispute, The Matter Will First Be Attempted To Be Resolved By A Sole Mediator Who Is A Neutral Third Party And Will Be Selected At The Mutual Acceptance Of A Proposed Mediator By Both Parties. Both Parties May Raise A Name For Sole Arbitrator And In The Case Both Parties Accept The Proposed Name, The Said Person Shall Be Appointed Sole Mediator. In The Case The Parties Are Not Able To Reach A Consensus Within Two Proposed Mediators, The Company Reserves The Right To Decide Who The Final Mediator Is. The Decision Of The Mediator Is Not Binding On Both Parties However; The Parties In Good Faith Will Attempt To Bind By The Decision.
Stage 2: Arbitration. In The Case That Mediation Does Not Yield A Result Suitable Or Preferred By Any One Of The Parties, Arbitration May Follow, The Award Of Which Is Binding On Both Parties. The Arbitration Board Is To Comprise Three Members. One Is To Be Appointed By Each Party And The Third Member Is To Be Nominated By The Two Appointed Members By Mutual Consent Between Them. The Award As The Outcome Of The Arbitration Is Final And Binding On Both Parties And There Shall Be No Further Remedy Available To Both Parties. The Arbitration Proceedings Will Take Place In The English Language And Will Be Situated In The State Of India. The Mode Of Appointment Of The Arbitrators Is As Provided Above.</p>
 <a href="Http://Www.Freeshopping.Co">Http://Www.Freeshopping.Co</a>
       <br/><br/><br/><br/>
    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="location.href='contact.php';">Contact Us</button>

    </div><br/><br/>

    <div class="footer-top-area" style="padding-top:0px;">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-about-us">
                        <h2>F<span>ree shopping</span></h2>
                        <p>FreeShopping Follows His Heart, Regardless Of Society’s Resistance And Rules!
                            While He Walks His Own Path, A FreeShopping Sees Limitless Inspiration To Change The World For Better And Make A Difference.
                            In All Our Diversities, There’s A Bit Of FreeShopping In Each One Of Us!</p>
                        <div class="footer-social">
                            <a href="https://www.facebook.com/freeshoppingpage" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/freeshoppingco" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCaAkF7F98rJ2rXdTf66v5hg" target="_blank"><i class="fa fa-youtube"></i></a>
                            <a href="https://in.linkedin.com/in/freeshopping" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <a href="https://www.instagram.com/freeshopping.co/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="https://in.pinterest.com/freeshoppingin/" target="_blank"><i class="fa fa-pinterest"></i></a>
                            <a href="http://freeshopping.co/" target="_blank"><i class="fa fa-google-plus"></i></a>
                            <a href="https://www.slideshare.net/freeshoppingonline1" target="_blank"><i class="fa fa-foursquare"></i></a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">GET TO KNOW US </h2>
                        <ul>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="ourteam.php">Our Team</a></li>
                            <li><a href="faq.php">FAQ's</a></li>
                            <li><a href="testimonial.php">Testimonials</a></li>
                            <li><a href="career.php">Career With Us</a></li>
                            <li><a href="media.php">Media Contact</a></li>
                            <li><a href="invest.php">Investors</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">SUPPORT</h2>
                        <ul>
                            <li><a href="advertise.php">Advertise With Us</a></li>
                            <li><a href="campus.php">Campus Ambassador</a></li>
                            <li><a href="feedback.php">Feedback</a></li>
                            <li><a href="giftcard.php">Win A Gift Cards</a></li>
                            <li><a href="tandc.php">Terms & Conditions</a></li>
                            <li><a href="pp.php">Privacy Policy</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-newsletter">
                        <h2 class="footer-wid-title">Never Miss Out Exclusive Deals !</h2>
                        <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                        <div class="newsletter-form">
                            <form action="#">
                                <input type="email" placeholder="Type your email">
                                <input type="submit" value="Subscribe">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p>Copyright 2015-2020. Freeshopping. All Rights Reserved. All Content, Trademarks And Logos Are Copyright Of Their Respective Owners. <a href="https://www.sahutechnologies.com/" target="_blank"> Sahu Technologies</a></p>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    
    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
    <script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>