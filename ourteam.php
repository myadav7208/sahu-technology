<?php
    session_start(); 
  
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OurTeam-Freeshoping</title>
    
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">

  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="user-menu">
                        <ul>
                            <?php if(isset($_SESSION['user'])) { ?>
                             <li><a href="logout.php" > Logout</a></li>
                                <li><h5>welcome <?php echo $_SESSION['name'] ?></h5></li> 
                            <?php } else{ ?>   
                            <li><a href="#Login" data-toggle="modal" data-target="#Login"> Login</a></li>
                                <div class="modal fade" id="Login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                        <div class="modal-body">
                                            <form action="login.php" method="POST">
                                                <input type="text" id="login" class="fadeIn second" name="email" placeholder="UserId">
                                                <input type="password" id="password" class="fadeIn third" name="pass" placeholder="password">
                                                <input type="submit" class="fadeIn fourth" value="Login" name="submit1">
                                            </form>
                                            

                                    </div>
                                 <div class="modal-footer">
                                    <a class="underlineHover" href="#" style="padding-right: 450px">Forgot Password ?</a>
                                    <a class="underlineHover" href="#Register" onclick="$('#Login').modal('hide')" data-toggle="modal" data-target="#Register" style="padding-right: 450px">Register</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                     </div>
                            <li><a href="#Register" data-toggle="modal" data-target="#Register"> Register</a></li>
                                    <div class="modal fade" id="Register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Register</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                        <div class="modal-body">
                                            <form action="register.php" method="POST">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                            <input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="last_name" id="last_name" class="form-control input-sm" placeholder="Last Name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address">
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
                                    </div>
                                </div>
                            </div>
                            
                            <input type="submit" value="Register" class="btn btn-info btn-block" name="submit">
                        
                        </form>
                                            

                                    </div>
                                 <div class="modal-footer">
                                    <a class="underlineHover" href="#" style="padding-right: 450px">Forgot Password ?</a>
                                    <a class="underlineHover" href="#" data-toggle="modal" data-target="#Login" style="padding-right: 465px" onclick="$('#Register').modal('hide')">Login</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                     </div>
                    <?php } ?>
                    <li><a href="career.php" > Career With Us</a></li>

                    </ul>
                    </div>
                </div>
                 <div class="row">
                <div class="col-md-4">
                    <div class="user-menu">
                        <ul>
                 <li><a href="https://www.facebook.com/freeshoppingpage" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/freeshoppingco" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCaAkF7F98rJ2rXdTf66v5hg" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="https://in.linkedin.com/in/freeshopping" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="https://www.instagram.com/freeshopping.co/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://in.pinterest.com/freeshoppingin/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="http://freeshopping.co/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="https://www.slideshare.net/freeshoppingonline1" target="_blank"><i class="fa fa-foursquare"></i></a></li>
                        </ul>
                </div>
                </div>
            </div>
                </div>
            </div>
        </div>
    </div>  <!-- End header area -->
    
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="./"><img src="img/logo.png"></a></h1>
                    </div>
                </div>
                
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="freebies.php">Freebies</a></li>
                        <li><a href="mobile_recharge.php">Mobile Recharge Offer</a></li>
                        <li><a href="electronic.php">Electronics Offer</a></li>
                        <li><a href="restaurant.php">Restaurant Offer</a></li>
                        <li><a href="travel_offer.php">Travels Offer</a></li>
                        <li><a href="groceries_offer.php">Groceries Offer</a></li>
                        <li><a hraf="student_offer.php" class="btn btn-warning btn-xs" style="border-radius: 30px;">Student Offer</a></li>
                    </ul>
                </div>  
            </div>
        </div>
    </div>

    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h3>Free Shopping-Official Site For Freebies, Offers, Coupons, Online Shopping India, Best Deals,Bollywood Promotion, And Free Stuff In India,Coupons,Discount Coupon Codes & Offers India,Promo Code, Cashback Offers, Coupon Code, Best Deals Online India</h3>
                    </div>
                </div>
            </div>
        </div>
    </div><br/><br/>

    <div class="container">
        <center><h2 style="color:tomato">About Us</h2></center>
        <p >Aren’t You Tired Of Collecting Coupons And Hunting The Internet For Deals And Promotional Codes? So Were We. And That’s The Reason, We Decided To Launch A Website That Makes It Easy For Shoppers To Find And Share The Best Deals, Offers And Coupons. No Hunting, No Hassle, And No Coupon Collection Required.<br/><br/>
Welcome To Freeshopping A Division Of  Sahu Technologies, We Are An Online Community Of Bargain Hunters, Who Love To Tracking Down The Top Deals, Offers And Coupons And Present It To You. Just Simply Get Yourself Registered With Us And Get  Access To Thousands Of Deals And Coupons. Also, You Will Be Updated As And When The Hottest Deals Hits Our Website. So We Assure You That You Won’t Be Missing Out On Any Upcoming Offers And Deals From Now On.<br/><br/>
We Are Passionate About Finding Things That Rise Above The Mundane; That We Think Will Catch Your Eye And Arrest It. Moreover, We Strive To Bring This To You, Wherever You Are And Whenever You Feel You Have The Time To Indulge A Little.So, We Established A Company To Bring You The Merchandise You Will Fall In Love With, To Give You A Chance To Create Your Own Space, To Find Coupons, Deals, Offers Where You Can Pay Less And Shop More. Also, To Help You Share Your Creations With Friends To Get That Second Opinion, To Inspire Or Be Inspired; And To Help You Shop In A Way That You Will Find Mesmerizing.<br/><br/>
We Are Totally Obsessed To Pick Out And Provide Awesome And Jaw Dropping Deals To Our Customers And This Obsession Drives And Motivates Us Every Single Day. Come Shop With Us.
        </p><br/>

<center><h2 style="color:tomato">PHILOSOPHY</h2></center>
        <p >FreeShopping Follows His Heart, Regardless Of Society’s Resistance And Rules!
While He Walks His Own Path, A FreeShopping Sees Limitless Inspiration To Change The World For Better And Make A Difference.
In All Our Diversities, There’s A Bit Of FreeShopping In Each One Of Us!
2 Years Back, One, TOO Stubborn Engineer, Followed Their Heart And Chose To Refuse Mindless High Paying Jobs To Become Entrepreneurs. Now Co-Founder Is Maheshwari Sahu.</p><br/>

<center><h2 style="color:tomato">The Concept Of Freeshopping</h2></center>
        <p >Freeshopping Was Established In 2015 And Is Based In Mumbai. We Aim To Help A Wide Range Of Shoppers To Avail Our Freebies, Coupons & Best Deals At The Lowest Prices. We Strive Hard To Remain Updated & Serve Our Customers Well So They Can Get The Advantages Of “Freeshopping” All The Time. We Are Passionate About Finding And Sharing Good Deals. Our Main Motto Is To Provide The Ease Of Shopping & Convenience To Our Customers At Affordable Prices.</p><br/>

<center><h2 style="color:tomato">What We Do?</h2></center>
        <p >We Aim To Assist Our Customers To Discover The Best Deals Online. We Aspire To Build A Company That Can Fulfill All Your Shopping Needs And Desires At One Stop Destination,  Where You Can Choose From A Wide Range Of Product Categories At Their Lowest Prices. Whether You Are Looking For Lowest Prices Or Discount Coupons, Whether It Is About Availing Freebies Or Contest Giveaways, We Provide It All.</p><br/>

<center><h2 style="color:tomato">Organizations Affiliated With Us</h2></center>
        <p >We Enjoy Working With Emerging E-Commerce Champs In Order To Promote Their Best Deals On Our Website.</p><br/>

        <h2 style="color:tomato" align="center">No Company Can Excel Without Brilliant And Like Inclined People, So Meet Our Rockstars Who Are Behind The Success Of Freeshopping.</h2>

       <strong> <h1 style="font-size: 80px; color:green;">Kindly Check Below<br> Keyword And Check Out</h1></strong><br/>
       <p>1) Searches related to free shopping<br/>
2) free shopping sites<br/>
3) free shopping online shopping<br/>
4) get free deals<br/>
5) online shopping offer free shipping<br/>
6) best offer online shopping today<br/>
7) free shopping free sample<br/>
8) cheapest online shopping cash on <br/>
9) best deals online shopping today<br/>
10) Searches related to best coupon website<br/>
11) coupon sites in india<br/>
12) online coupon websites<br/>
13) best coupon sites 2018<br/>
14) coupon codes websites<br/>
15) coupon websites like groupon<br/>
16) coupon websites uk<br/>
17) coupon india<br/>
18) Searches related to best freebies website<br/>
19) free samples websites<br/>
20) legit freebie sites<br/>
21) best free samples sites<br/>
22) woman freebies<br/>
23) freebies<br/>
24) freebies free shopping<br/>
25) freestuff free shopping<br/>
26) my free product samples<br/>
27) 10 Best Legit Websites to Get Free Stuff<br/> 
28) Best Websites To Get Free Stuff And Samples<br/>
29) 7 Sites to Get Free Samples Without Filling Out Surveys<br/>
30) my free product samples<br/>
31) free beauty product samples<br/>
32) free sample product home delivery<br/>
33) free samples by mail free shipping<br/>
34) absolutely free stuff no participation required<br/>
35) free samples by mail no requirements<br/>
36) totally free stuff no strings attached<br/>
37) free sample boxes<br/>
38) free ka maal online shopping<br/>
39) free shopping sites<br/>
40) online shopping offer free shipping<br/>
41) best offer online shopping today<br/>
42) freekaamaal free sample<br/>
43) cheapest online shopping cash on delivery<br/>
44) best deals online shopping today<br/>
45) online shopping with best discounts<br/>
46) coupon sites in india<br/>
47) discount coupons for amazon<br/>
48) couponzguru flipkart<br/>
49) discount coupons for flipkart<br/>
50) free shopping amazon<br/>
51) best offer online shopping today<br/>
52) discount coupons for ajio<br/>
53) online shopping with best discounts<br/>
54) Searches related to free offers website<br/>
55) free ka maal online shopping<br/>
56) best offer online shopping today<br/>
57) free shopping sites<br/>
58) get free deals<br/>
59) online shopping offer free shipping<br/>
60) online shopping with best discounts<br/>
61) best deals online shopping today<br/>
62) freekaamaal free sample<br/>
63) discount coupons for amazon<br/>
64) discount coupons for flipkart<br/>
65) coupon sites in india<br/>
66) discount coupons for myntra<br/>
67) coupon code paytm<br/>
68) coupon india<br/>
69) promo codes for free stuff<br/>
70) coupons freecharge<br/>
71) coupon sites in india<br/>
72) discount coupons for amazon<br/>
73) coupon code dominos<br/>
74) discount deals for flipkart<br/>
75) promo codes for free stuff<br/>
76) coupon india<br/>
77) discount coupons for klassyink<br/>
78) coupon code paytm<br/>
79) discount offer for amazon<br/>
80) discount offer for flipkart<br/>
81) discount offer for myntra<br/>
82) coupons paytm<br/>
83) coupon sites in india<br/>
84) coupon india<br/>
85) coupons pizza<br/>
86) coupon code dominos<br/>
87) discount coupons for amazon<br/>
88) discount coupons for flipkart<br/>
89) coupon sites in india<br/>
90) coupon india<br/>
91) discount coupons for free shopping<br/>
92) coupon code free shopping<br/>
93) coupons free shopping<br/>
94) coupon code dominos<br/>
95) cashback online shopping sites<br/>
96) top 10 cashback sites in india<br/>
97) best cashback sites for mobile recharge<br/>
98) cashback free shopping india<br/>
99) free shopping coupon<br/>
100) cashback free shopping<br/>
101) free shopping app<br/>
102) free shopping<br/>
103) Which is India’s No. 1 highest paying cashback site<br/>
104) coupon websites India<br/>
105) coupon websites USA</p>




    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="location.href='contact.php';">Contact Us</button>

    </div><br/><br/>




    <div class="footer-top-area" style="padding-top:0px;">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-about-us">
                        <h2>F<span>ree shopping</span></h2>
                        <p>FreeShopping Follows His Heart, Regardless Of Society’s Resistance And Rules!
                            While He Walks His Own Path, A FreeShopping Sees Limitless Inspiration To Change The World For Better And Make A Difference.
                            In All Our Diversities, There’s A Bit Of FreeShopping In Each One Of Us!</p>
                        <div class="footer-social">
                            <a href="https://www.facebook.com/freeshoppingpage" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/freeshoppingco" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCaAkF7F98rJ2rXdTf66v5hg" target="_blank"><i class="fa fa-youtube"></i></a>
                            <a href="https://in.linkedin.com/in/freeshopping" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <a href="https://www.instagram.com/freeshopping.co/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="https://in.pinterest.com/freeshoppingin/" target="_blank"><i class="fa fa-pinterest"></i></a>
                            <a href="http://freeshopping.co/" target="_blank"><i class="fa fa-google-plus"></i></a>
                            <a href="https://www.slideshare.net/freeshoppingonline1" target="_blank"><i class="fa fa-foursquare"></i></a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">GET TO KNOW US </h2>
                        <ul>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="ourteam.php">Our Team</a></li>
                            <li><a href="faq.php">FAQ's</a></li>
                            <li><a href="testimonial.php">Testimonials</a></li>
                            <li><a href="career.php">Career With Us</a></li>
                            <li><a href="media.php">Media Contact</a></li>
                            <li><a href="invest.php">Investors</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">SUPPORT</h2>
                        <ul>
                            <li><a href="advertise.php">Advertise With Us</a></li>
                            <li><a href="campus.php">Campus Ambassador</a></li>
                            <li><a href="feedback.php">Feedback</a></li>
                            <li><a href="giftcard.php">Win A Gift Cards</a></li>
                            <li><a href="tandc.php">Terms & Conditions</a></li>
                            <li><a href="pp.php">Privacy Policy</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-newsletter">
                        <h2 class="footer-wid-title">Never Miss Out Exclusive Deals !</h2>
                        <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                        <div class="newsletter-form">
                            <form action="#">
                                <input type="email" placeholder="Type your email">
                                <input type="submit" value="Subscribe">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p>Copyright 2015-2020. Freeshopping. All Rights Reserved. All Content, Trademarks And Logos Are Copyright Of Their Respective Owners. <a href="https://www.sahutechnologies.com/" target="_blank"> Sahu Technologies</a></p>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    
    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
    <script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>