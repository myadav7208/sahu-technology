<?php
    session_start(); 

    $con = mysqli_connect('localhost', 'root', '');

    $db = mysqli_select_db($con, 'sahu');

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Freeshopping</title>
    
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">

    <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    
  </head>
  <body>
    <div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="user-menu">
                        <ul>
                            <?php if(isset($_SESSION['user'])) { ?>
                             <li><a href="logout.php" > Logout</a></li>
                                <li><h5>welcome <?php echo $_SESSION['name']; ?></h5></li> 
                            <?php } else{ ?>  
                            <?php if(!isset($_SESSION['user'])) { sleep(1);

                             ?> 
                            <li><span class="glyphicon glyphicon-lock" style="font-size: 10px;
                            margin-right: 0px;"></span></li> <?php } else { ?>
                            <li><span class="glyphicon glyphicon-home" style="font-size: 10px;
                            margin-right: 0px;"></span></li> <?php } ?>
                                <li>
                                    <a href="#Login" data-toggle="modal" data-target="#Login"> Login</a></li>
                                <div class="modal fade" id="Login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                        <div class="modal-body">
                                            <form action="login.php" method="POST">
                                                <input type="text" id="userid" class="fadeIn second" name="email" placeholder="UserId">
                                                <input type="password" id="password" class="fadeIn third" name="pass" placeholder="password">
                                                <input type="submit" class="fadeIn fourth" value="Login" name="submit1" id="submit" onclick="popfun()">
                                            </form>
                                            

                                    </div>
                                 <div class="modal-footer">
                                    <a class="underlineHover" href="#" style="padding-right: 450px">Forgot Password ?</a>
                                    <a class="underlineHover" href="#Register" onclick="$('#Login').modal('hide')" data-toggle="modal" data-target="#Register" style="padding-right: 450px">Register</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                     </div>
                     <li><span class="glyphicon glyphicon-user" style="font-size: 10px;
                            margin-right: 0px;"></span></li>
                            <li><a href="#Register" data-toggle="modal" data-target="#Register" > Register</a></li>
                                    <div class="modal fade" id="Register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Register</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                        <div class="modal-body">
                                            <form action="register.php" method="POST">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                            <input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="last_name" id="last_name" class="form-control input-sm" placeholder="Last Name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address">
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
                                    </div>
                                </div>
                            </div>
                            
                            <input type="submit" value="Register" class="btn btn-info btn-block" name="submit" >
                        
                        </form>
                                            

                                    </div>
                                 <div class="modal-footer">
                                    <a class="underlineHover" href="#" style="padding-right: 450px">Forgot Password ?</a>
                                    <a class="underlineHover" href="#" data-toggle="modal" data-target="#Login" style="padding-right: 465px" onclick="$('#Register').modal('hide')">Login</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                     </div>
                    <?php } ?>

                    <li><a href="career.php" > Career With Us</a></li>

                    </ul>
                    </div>
                    
        </div>

        <div class="row">
                <div class="col-md-4">
                    <div class="user-menu">
                        <ul>
                 <li><a href="https://www.facebook.com/freeshoppingpage" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/freeshoppingco" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCaAkF7F98rJ2rXdTf66v5hg" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="https://in.linkedin.com/in/freeshopping" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="https://www.instagram.com/freeshopping.co/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://in.pinterest.com/freeshoppingin/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="http://freeshopping.co/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="https://www.slideshare.net/freeshoppingonline1" target="_blank"><i class="fa fa-foursquare"></i></a></li>
                        </ul>
                </div>
                </div>
            </div>

                </div>


            </div>
            
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="./"><img src="img/logo.png"></a></h1>
                    </div>
                </div>
                
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area fluid-container">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="freebies.php">Freebies</a></li>
                        <li><a href="mobile_recharge.php">Mobile Recharge Offer</a></li>
                        <li><a href="electronic.php">Electronics Offer</a></li>
                        <li><a href="restaurant.php">Restaurant Offer</a></li>
                        <li><a href="travel_offer.php">Travels Offer</a></li>
                        <li><a href="groceries_offer.php">Groceries Offer</a></li>
                        <li><a hraf="student_offer.php" class="btn btn-warning btn-xs" style="border-radius: 30px;">Student Offer</a></li>
                    </ul>
                </div>  
            </div>
        </div>
    </div> <!-- End mainmenu area -->
    
    
    <div class="slider-area fluid-container">
        	<!-- Slider -->
			<div class="block-slider block-slider4">
				<ul class="" id="bxslider-home4">
					<li>
						<img src="img/h4-slide.png" alt="Slide">
					</li>
					<li><img src="img/h4-slide2.png" alt="Slide">
					</li>
					<li><img src="img/h4-slide3.png" alt="Slide">
					</li>
					<li><img src="img/h4-slide4.png" alt="Slide">
					</li>
				</ul>
			</div>
			<!-- ./Slider -->
    </div> <!-- End slider area -->
    
    <div class="maincontent-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="latest-product">
                        <h2 class="section-title">Latest Products</h2>
                        <hr style=" border-style: inset; border-width: 1px;"></hr>
                        <div class="row">
                            <?php
                               //$count = 1;
                                $query = "select * from freebies";
                                $result = mysqli_query($con,$query);
                                
                                while($row = mysqli_fetch_array($result))
                                {
                                  
                                ?>

                            <div class="single-product col-md-3" style="padding:20px;border-radius: 4px" >
                                <div class="product-f-image" >
                                    <?php echo '<img style="width:100%;height:300px; box-shadow:6px 6px 6px 6px #cec5b6; border-radius:5px" src="data:image;base64,'.base64_encode($row['pimage']).'">'; ?>
                                   
                                    <div class="product-hover">
                                        <?php if(!isset($_SESSION['user']))
                                        { ?>
                                        <a href="#" class="add-to-cart-link"><span class="glyphicon glyphicon-eye-open"></span>Login for coupon</a> <?php } else { ?>

                                        <a href="#" class="add-to-cart-link"><span class="glyphicon glyphicon-eye-open"></span><?php echo $row['pcoupon']; ?></a> <?php } ?>

                                         <a href="#detail" class="view-details-link" data-toggle="modal" data-target="#detail"><i class="fa fa-link"></i>See Deatails</a>

                                         <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                         <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="detail">Product Description</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                <?php echo $row['pdetail']; ?>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                </br>
                                 
                            </div>
                        <?php } ?>
                        </div>
                   </div>
                </div>
            </div>
        </div>
    </div>

<div class="container">
    <center><nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <li class="page-item disabled">
      <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
    </li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">4</a></li>
    <li class="page-item"><a class="page-link" href="#">5</a></li>
    <li class="page-item">
      <a class="page-link" href="#">Next</a>
    </li>
  </ul>
</nav></center>

</div>



<!-- Modal -->

    <!-- End main content area -->


    
     <!-- End product widget area -->
    
    <div class="footer-top-area" style="padding-top:0px;">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-about-us">
                        <h2>F<span>ree shopping</span></h2>
                        <p>FreeShopping Follows His Heart, Regardless Of Society’s Resistance And Rules!
                            While He Walks His Own Path, A FreeShopping Sees Limitless Inspiration To Change The World For Better And Make A Difference.
                            In All Our Diversities, There’s A Bit Of FreeShopping In Each One Of Us!</p>
                        <div class="footer-social">
                            <a href="https://www.facebook.com/freeshoppingpage" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/freeshoppingco" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCaAkF7F98rJ2rXdTf66v5hg" target="_blank"><i class="fa fa-youtube"></i></a>
                            <a href="https://in.linkedin.com/in/freeshopping" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <a href="https://www.instagram.com/freeshopping.co/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="https://in.pinterest.com/freeshoppingin/" target="_blank"><i class="fa fa-pinterest"></i></a>
                            <a href="http://freeshopping.co/" target="_blank"><i class="fa fa-google-plus"></i></a>
                            <a href="https://www.slideshare.net/freeshoppingonline1" target="_blank"><i class="fa fa-foursquare"></i></a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">GET TO KNOW US </h2>
                        <ul>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="ourteam.php">Our Team</a></li>
                            <li><a href="faq.php">FAQ's</a></li>
                            <li><a href="testimonial.php">Testimonials</a></li>
                            <li><a href="career.php">Career With Us</a></li>
                            <li><a href="media.php">Media Contact</a></li>
                            <li><a href="invest.php">Investors</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">SUPPORT</h2>
                        <ul>
                            <li><a href="advertise.php">Advertise With Us</a></li>
                            <li><a href="campus.php">Campus Ambassador</a></li>
                            <li><a href="feedback.php">Feedback</a></li>
                            <li><a href="giftcard.php">Win A Gift Cards</a></li>
                            <li><a href="tandc.php">Terms & Conditions</a></li>
                            <li><a href="pp.php">Privacy Policy</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-newsletter">
                        <h2 class="footer-wid-title">Never Miss Out Exclusive Deals !</h2>
                        <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                        <div class="newsletter-form">
                            <form action="#">
                                <input type="email" placeholder="Type your email">
                                <input type="submit" value="Subscribe">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p>Copyright 2015-2020. Freeshopping. All Rights Reserved. All Content, Trademarks And Logos Are Copyright Of Their Respective Owners. <a href="https://www.sahutechnologies.com/" target="_blank"> Sahu Technologies</a></p>
                    </div>
                </div>   
            </div>
        </div>
    </div> 


    <?php
        echo '<script type="text/javascript">';
        echo 'function popfun()
        {  
             swal("Good job!", "You are loged in successfully!", "success");
              
        }
         </script>';
     ?>


    <!-- End footer bottom area -->
   
    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    
    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>

    

  </body>
</html>