<?php
    session_start(); 
   
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FAQ-Freeshoping</title>
    
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">

  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="user-menu">
                        <ul>
                            <?php if(isset($_SESSION['user'])) { ?>
                             <li><a href="logout.php" > Logout</a></li>
                                <li><h5>welcome <?php echo $_SESSION['name'] ?></h5></li> 
                            <?php } else{ ?>   
                            <li><a href="#Login" data-toggle="modal" data-target="#Login"> Login</a></li>
                                <div class="modal fade" id="Login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                        <div class="modal-body">
                                            <form action="login.php" method="POST">
                                                <input type="text" id="login" class="fadeIn second" name="email" placeholder="UserId">
                                                <input type="password" id="password" class="fadeIn third" name="pass" placeholder="password">
                                                <input type="submit" class="fadeIn fourth" value="Login" name="submit1">
                                            </form>
                                            

                                    </div>
                                 <div class="modal-footer">
                                    <a class="underlineHover" href="#" style="padding-right: 450px">Forgot Password ?</a>
                                    <a class="underlineHover" href="#Register" onclick="$('#Login').modal('hide')" data-toggle="modal" data-target="#Register" style="padding-right: 450px">Register</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                     </div>
                            <li><a href="#Register" data-toggle="modal" data-target="#Register"> Register</a></li>
                                    <div class="modal fade" id="Register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Register</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                        <div class="modal-body">
                                            <form action="register.php" method="POST">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                            <input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="last_name" id="last_name" class="form-control input-sm" placeholder="Last Name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address">
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
                                    </div>
                                </div>
                            </div>
                            
                            <input type="submit" value="Register" class="btn btn-info btn-block" name="submit">
                        
                        </form>
                                            

                                    </div>
                                 <div class="modal-footer">
                                    <a class="underlineHover" href="#" style="padding-right: 450px">Forgot Password ?</a>
                                    <a class="underlineHover" href="#" data-toggle="modal" data-target="#Login" style="padding-right: 465px" onclick="$('#Register').modal('hide')">Login</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                     </div>
                    <?php } ?>
                    <li><a href="career.php" > Career With Us</a></li>

                    </ul>
                    </div>
                </div>
                 <div class="row">
                <div class="col-md-4">
                    <div class="user-menu">
                        <ul>
                 <li><a href="https://www.facebook.com/freeshoppingpage" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/freeshoppingco" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCaAkF7F98rJ2rXdTf66v5hg" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="https://in.linkedin.com/in/freeshopping" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="https://www.instagram.com/freeshopping.co/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://in.pinterest.com/freeshoppingin/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="http://freeshopping.co/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="https://www.slideshare.net/freeshoppingonline1" target="_blank"><i class="fa fa-foursquare"></i></a></li>
                        </ul>
                </div>
                </div>
            </div>
                </div>
            </div>
        </div>
    </div>  <!-- End header area -->
    
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="./"><img src="img/logo.png"></a></h1>
                    </div>
                </div>
                
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="freebies.php">Freebies</a></li>
                        <li><a href="mobile_recharge.php">Mobile Recharge Offer</a></li>
                        <li><a href="electronic.php">Electronics Offer</a></li>
                        <li><a href="restaurant.php">Restaurant Offer</a></li>
                        <li><a href="travel_offer.php">Travels Offer</a></li>
                        <li><a href="groceries_offer.php">Groceries Offer</a></li>
                        <li><a hraf="student_offer.php" class="btn btn-warning btn-xs" style="border-radius: 30px;">Student Offer</a></li>
                    </ul>
                </div>  
            </div>
        </div>
    </div>

    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Frequently Asked Question (FAQ)</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- faq start -->
    <div class="container">
    <br />
    <br />
    <br />
    <br />

    <div class="" id="accordion">
        <center><h2>General Questions</h2></center>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">(Q) What Is Freeshopping.Co?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="card-block">
                    <h4>Freeshopping.Co Is An Online Portal Which Provides The Shoppers To Avail Our Freebies, Coupons & Best Deals At The Lowest Prices.</h4><br>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Q) What Can I Do On Freeshopping.Co?</a>
                </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse">
                <div class="card-block">
                    <h4>You Can Search The Product You Are Looking For Right Into The Search Box And We Will Arrive At A List Of Your Favourites Brands, Prices And Offers From Various Websites Right In Front Of You. You Can Then Compare The Prices, Budgets And Offers And Rightly Choose Your Desired Product.</h4><br>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">Q) Am I Shopping On Your Website Directly?</a>
                </h4>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse">
                <div class="card-block">
                    <h4>We Know That This Question Has Been Running On Your Mind From A Long Time. But, The Answer Is A “NO”. We Are A Marketing And Branding Platform That Advertises The Famous Websites And You Will Be Redirected To That Particular Website From Which You Chose The Offer Or The Product. By Doing So, You Are Directly And Greatly Benefited From The Seller!</h4><br>
                </div>
            </div>
        </div>

        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">(Q) What Offers Can I Get On Freeshopping.Co ?</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="card-block">
                    <h4>We Search And Provide The Best Deals, Offers And Prices From Different Website At A Single Place; So That, You Can Choose The Best Deal And Grab It Hot!</h4><br>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Q) What Products Does Freeshopping.Co Offer Me?</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="card-block">
                    <h4>Freeshopping.Co Offers You With Everything At One Place And Facilitates Easy Buying Of Books, Mobiles & Accessories, Computers, Cameras & Accessories, Gaming, Music, Movies & Posters, TV & Audio/Video Player, E Books, Home & Kitchen, Pens & Stationery, Sunglasses, Jewellery, Watches, Bags, Wallets & Belts, Sports & Fitness, Footwear, Beauty And Personal Care, Baby Care, Toys, Clothing, Home Furnishing And Many More.
Now, We Have Something Special And Customised For You, If You Don’t Have The Time And Patience To Browse, Compare And Decide…
Just Inbox Us With Your Requirement And Specifications By Mailing To Freeshopping.Official@Gmail.Com And We Will Respond With Your Product Of Choice, Best Deals From Best Sellers Within 10 Mins Of Your Precious Time. This Will Make It Easier For You To Buy The Best.</h4><br>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">(Q) Do I Need To Register?</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="card-block">
                    <h4>Registration Is Not Mandatory, But There Are Certain Privileges If You Do Get Registered. You Will Be Updated As And When There Are Hot Deals Available On Our Website. Also, As And When Any Awesome Deal, Coupon Or Offer Is Added With Us, Then You Will Be Informed Before The Others.</h4><br>
                    <br />
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">(Q) How Will Freeshopping.Co Display My Products Or Page?</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="card-block">
                    <h4>We, At Freeshopping.Co Give The Freedom And Ease To Our Clients To Select The Areas Wherever They Wish To Be Projected On Our Website.
We Offer ‘Banner Advertising’ Which Is Our Top Featured Location And Provides The Best Visibility To The Customers.
Also,There Are “Home Page Offers” And “Top Deals Of The Week’.<br>
You Can Choose Any Of These Slots Wherein You Want Your Product Or Website To Be Displayed.
                    <ul>
                        <li> Banner Advertising</li>
                        <li> Home Page Advertising</li>
                        <li>Top Deals Advertising</li>
                        <li>Custom Designed Promotions Package</li>
                        <li>Blogs</li>
                        <li>Coupon Promotions</li>
                        <li>Contests And Freebies</li>
                    </ul></h4><br>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">(Q) Who Can Get Associated With Freeshopping.Co?</a>
                </h4>
            </div>
            <div id="collapseEight" class="panel-collapse collapse">
                <div class="card-block">
                    <h4>Any Emerging Business/Merchant Who Wishes To List His/Her Products Or Any Full Fledged E-Commerce Website Could Be An Affiliate Partner With Freeshopping.Co.</h4><br> 
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">(Q) How Can I Promote My Offers On Freeshopping.Co?</a>
                </h4>
            </div>
            <div id="collapseNine" class="panel-collapse collapse">
                <div class="card-block">
                    <h4>We, At Freeshopping.Co Provide Innumerable Options To Our Partners For Promoting Their Offers And Website Which Are Best Suited To Their Needs.Our Options Ranges From Listing Your Offers On Our Website, Efficiently Adding Your Coupons In Our Store, Banner Advertising, Social Media Marketing And More.Once Your Store Is Added To Our Portal, You Can Easily Add Your Coupons From Your End Too.</h4><br>
                </div>
            </div>
        </div>

        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Contact Us</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="card-block">
                    <h4>If You Have Any Queries, Complaints Or Feedbacks, You Can Contact Us On The Below Mentioned Email ID Freeshopping.Official@Gmail.Com  Or  Info@Freeshopping.Co And We Will Be Sure To Get Back With Assistance To You As Early As Possible.
                    <br/><br/>
                     <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="location.href='contact.php';">Contact Us</button>
                    </h4><br>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>






<!--footer -->

<div class="footer-top-area" style="padding-top:0px;">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-about-us">
                        <h2>F<span>ree shopping</span></h2>
                        <p>FreeShopping Follows His Heart, Regardless Of Society’s Resistance And Rules!
                            While He Walks His Own Path, A FreeShopping Sees Limitless Inspiration To Change The World For Better And Make A Difference.
                            In All Our Diversities, There’s A Bit Of FreeShopping In Each One Of Us!</p>
                        <div class="footer-social">
                            <a href="https://www.facebook.com/freeshoppingpage" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/freeshoppingco" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCaAkF7F98rJ2rXdTf66v5hg" target="_blank"><i class="fa fa-youtube"></i></a>
                            <a href="https://in.linkedin.com/in/freeshopping" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <a href="https://www.instagram.com/freeshopping.co/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="https://in.pinterest.com/freeshoppingin/" target="_blank"><i class="fa fa-pinterest"></i></a>
                            <a href="http://freeshopping.co/" target="_blank"><i class="fa fa-google-plus"></i></a>
                            <a href="https://www.slideshare.net/freeshoppingonline1" target="_blank"><i class="fa fa-foursquare"></i></a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">GET TO KNOW US </h2>
                        <ul>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="ourteam.php">Our Team</a></li>
                            <li><a href="faq.php">FAQ's</a></li>
                            <li><a href="testimonial.php">Testimonials</a></li>
                            <li><a href="career.php">Career With Us</a></li>
                            <li><a href="media.php">Media Contact</a></li>
                            <li><a href="invest.php">Investors</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">SUPPORT</h2>
                        <ul>
                            <li><a href="advertise.php">Advertise With Us</a></li>
                            <li><a href="campus.php">Campus Ambassador</a></li>
                            <li><a href="feedback.php">Feedback</a></li>
                            <li><a href="giftcard.php">Win A Gift Cards</a></li>
                            <li><a href="tandc.php">Terms & Conditions</a></li>
                            <li><a href="pp.php">Privacy Policy</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-newsletter">
                        <h2 class="footer-wid-title">Never Miss Out Exclusive Deals !</h2>
                        <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                        <div class="newsletter-form">
                            <form action="#">
                                <input type="email" placeholder="Type your email">
                                <input type="submit" value="Subscribe">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p>Copyright 2015-2020. Freeshopping. All Rights Reserved. All Content, Trademarks And Logos Are Copyright Of Their Respective Owners. <a href="https://www.sahutechnologies.com/" target="_blank"> Sahu Technologies</a></p>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    
    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
    <script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>